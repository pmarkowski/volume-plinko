const Matter = require('matter-js');

// module aliases
var Engine = Matter.Engine,
    Render = Matter.Render,
    World = Matter.World,
    Bodies = Matter.Bodies,
    MouseConstraint = Matter.MouseConstraint,
    Mouse = Matter.Mouse;

// parameters
const discRadius = 20;
const numPlinkoBuckets = 9;
const bucketWidth = 2.25 * discRadius; // make it a bit wider so it can fit
const bucketWallThickness = 0.25 * discRadius; // iunno this seems reasonable.
const plinkoLayers = 13; // how many layers of pegs
const areaWidth = (bucketWidth + bucketWallThickness * 2) * numPlinkoBuckets;
const pegRadius = 2;

// create an engine
var engine = Engine.create();

// create a renderer
var render = Render.create({
    element: document.body,
    engine: engine,
    options: {
        width: areaWidth,
        height: 1000
    }
});


var pegs = [];

var y = 30; // initial vertical offset
var horOffset = 30; // width of wall: todo make the walls
for (let plinkoLayer = 0; plinkoLayer < plinkoLayers; plinkoLayer++) {
    if (plinkoLayer % 2 === 0) {
        var numPegs = numPlinkoBuckets - 1;
        y += bucketWidth;

        for (let j = 0; j < numPegs; j++) {
            var x = j * bucketWidth + horOffset;
            pegs.push(Bodies.circle(x, y, pegRadius, { isStatic: true }));
        }
    }
    else
    {
        var numPegs = numPlinkoBuckets - 2;
        // Edges should point inwards though
        y += bucketWidth;

        for (let j = 0; j < numPegs; j++) {
            var x = j * bucketWidth + (bucketWidth / 2) + horOffset;
            pegs.push(Bodies.circle(x, y, pegRadius, { isStatic: true }));
        }
    }
}

// create two boxes and a ground
var plinkoDisc = Bodies.circle(400, 200, discRadius, { restitution: 0.99 });
var plinkoWall1 = Bodies.rectangle(0 + (bucketWallThickness/2), 610 - 30 - bucketWidth/2, bucketWallThickness, bucketWidth, { isStatic: true });
var ground = Bodies.rectangle(400, 910, 810, 60, { isStatic: true });

// add all of the bodies to the world
World.add(engine.world, [...pegs, plinkoDisc, plinkoWall1, ground]);

// add mouse control
var mouse = Mouse.create(render.canvas),
    mouseConstraint = MouseConstraint.create(engine, {
        mouse: mouse,
        constraint: {
            stiffness: 0.2,
            render: {
                visible: false
            }
        }
    });

World.add(engine.world, mouseConstraint);

// run the engine
Engine.run(engine);

// run the renderer
Render.run(render);